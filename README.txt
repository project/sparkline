
Welcome to sparkline.module.  This module is designed as a Drupal wrapper
for the PHP Sparkline library[1].  It allows other modules to generate
sparklines[2], and provides a filter for users to embed them in posts.

FEATURES
--------
* Creates cool sparkline images.
* Filters nodes to display embedded sparklines.
* More to be written.


INSTALL REQUIREMENTS
--------------------
1.  Requires the PHP binary to have been built with the GD library, which is
    used by the PHP Sparkline library for generating the sparklines images. A
    version is included with this module as it requires some changes to not
    conflict with Drupal functions.

BUGS / TO DO
------------
To find, search, and contribute to bugs and new features for htis module, see
the project's issue queue on drupal.org:

http://drupal.org/project/issues/sparkline

CONTRIBUTORS
------------
* Original prototype developed by Jeff Eaton (#drupal:eaton).
* Further development by Chris Johnson (#drupal:chrisxj, http://drupal.org/user/2794).
